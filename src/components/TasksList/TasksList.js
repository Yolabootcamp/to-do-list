import React from "react";
import Tasks from "../Tasks/Tasks";

export default function TasksList({todos, setTodos, filteredTodo}){

    return(
        <div className="tasks-list">
            <ul className="tasks-lists">
                {filteredTodo.map((todo) => (
                    <Tasks key={todo.id}
                           text={todo.text}
                           todo={todo}
                           todos={todos}
                           setTodos={setTodos}
                           filteredTodo={filteredTodo}/>
                ))}
            </ul>
        </div>
    );
}