import React from "react";

export default function Header() {
  return (
    <div>
      <header>
        <h1>Yolanda's To Do list</h1>
      </header>
    </div>
  );
}
