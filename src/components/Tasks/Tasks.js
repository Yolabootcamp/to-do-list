import React,{useState} from "react";
import { UilTimesCircle } from '@iconscout/react-unicons';
import { UilCheckCircle } from '@iconscout/react-unicons';
import { UilEdit } from '@iconscout/react-unicons';

export default function Tasks({ text, todo, todos, setTodos, editTodo}) {

    const handleDelete = () => {
        setTodos(todos.filter((task) => task.id !== todo.id));
    };

    const handleCompleted = () => {
        setTodos(todos.map((elem) => {
                if (elem.id === todo.id) {
                    return {
                        ...elem, completed: !elem.completed
                    }
                }
                return elem;
            }
        ))
    }

    return(

        <div className="todos-container">
            <li className={`todos-row ${todo.completed ? "completed-btn" : ""}`}>{text}</li>
            <div className="todos-icons">
                <UilCheckCircle className="task-check-icon" onClick={handleCompleted}/>
                <UilTimesCircle className="task-delete-icon" onClick={handleDelete} />
            </div>

        </div>
    );
}