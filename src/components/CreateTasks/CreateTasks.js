import React, {useState, useEffect, useRef} from "react";
import TasksList from "../TasksList/TasksList";
import { UilPlusCircle } from '@iconscout/react-unicons';
import { UilAngleDown } from '@iconscout/react-unicons';

export default function CreateTasks({ todos, setTodos, addTodos, setStatus}){

    const [inputText, setInputText] = useState('');

    const handleInputText = (event) =>{
        setInputText(event.target.value);
    }

    const handleSaveTask = (event) =>{
        event.preventDefault();
        setTodos((prevTasks) => ([
            ...prevTasks,
            {
                text:inputText,
                completed: false,
                id:Math.floor(Math.random() * 10),
            }
        ]));

        setInputText("");
    }

    const inputRef = useRef();
    useEffect(() => {
        inputRef.current.focus();
    }, []);

    const handleStatus = (event) => {
        setStatus(event.target.value);
    };

  return(
      <div className="form">
          <h2>What's the plan for today? </h2>

          <form className="todo-form" onSubmit={handleSaveTask}>

        <input
            type="text"
            name="inputText"
            value={inputText}
            onChange={handleInputText}
            className="form-input"
            placeholder="Add a todo"
            ref={inputRef}
        />
              <button className="form-button" type="submit">
                  <UilPlusCircle />
              </button>

        <div>
          <select onChange={handleStatus} name="todos" className="filter-todos">
              <UilAngleDown className="arrow"/>
            <option value="all" className="options">All</option>
            <option value="completed">Completed</option>
            <option value="uncompleted">Uncompleted</option>
          </select>
        </div>
      </form>
      </div>
  );

}