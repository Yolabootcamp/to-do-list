import React, {useEffect, useState} from "react";
import Header from "./components/Header/Header";
import CreateTasks from "./components/CreateTasks/CreateTasks";
import TasksList from './components/TasksList/TasksList';
import "./styles/style.css";
import Footer from "./components/Footer/Footer";

function App() {

    const [todos, setTodos] = useState([]);
    const [filteredTodo, setFilteredTodo] = useState([]);
    const [status, setStatus] = useState("All");

    useEffect(() => {
        handleFiltered();
    }, [todos, status]);

    const handleFiltered = () => {
        switch (status) {
            case "completed":
                setFilteredTodo(todos.filter((todo) => todo.completed === true));
                break;
            case "uncompleted":
                setFilteredTodo(todos.filter((todo) => todo.completed === false));
                break;
            default:
                setFilteredTodo(todos);
                break;
        }
    };
    return (
        <>
        <Header />

    <div className="App">

        <CreateTasks todos={todos} setTodos={setTodos} setStatus={setStatus} />
        <TasksList todos={todos} setTodos={setTodos} filteredTodo={filteredTodo} />
    </div>
            <Footer />
            </>
  );
}

export default App;
