                                                TO DO LIST

Main Goals:
1- Inputs to create a to do list
2- Store the todos in an array and manage them with state
3- Be able to delete them
4- Be able to check the todos
5- Be able to filter all, completed and uncompleted todos

For this project I used programming languages HTML, CSS and JavaScript, using the React.js library, the IDE I often use is VS Code. I created a several components for the code.
So you can use the app on your computer. But first you have to install the node.js insIde the folder that you will open the app from react.js.
The clone for this site you can find it on my gitlab and bit bucket account, using the following link:
https://gitlab.com/Yolabootcamp/keeper.git.
Thank you.

The css was based on video and also to bill this app, I based on a YouTube videos.

I'm thinking to update this app soon, for more functionalities.